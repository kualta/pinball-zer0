﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class SpringController : MonoBehaviour
{
    public delegate void activeState();
    public activeState ActiveState;

    public float springMaxScaleZ = 1.744722f;
    public float springMinScaleZ = 0.8f;
    public float springPower;
    public float powerMultiplier = 45f;

    public void setState(Action state) {
        ActiveState = new activeState(state);
    }

    public void Compress() {
        transform.localScale -= new Vector3(0, 0, 0.9f) * Time.deltaTime * 20;
    }

    public void Release() {
        transform.localScale += new Vector3(0, 0, 0.9f) * Time.deltaTime * 100;
    }

    public void Idle() {

    }

    void Start() {
        setState(Idle);
    }

    void Update() {
        springPower = (springMaxScaleZ - transform.localScale.z) * powerMultiplier;
        ActiveState();
    }
}
