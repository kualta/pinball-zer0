﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    GameObject controllerObj;
    GameController controller;

    public Rigidbody rigidbody;
    public bool springConnected;

    void Start()
    {
        springConnected = false;
        controllerObj = GameObject.Find("Controller");
        controller = controllerObj.GetComponent<GameController>();
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate() {
        rigidbody.AddForce(Vector3.down);

        RaycastHit hit;

        if (Physics.Raycast(transform.position, -controller.board.transform.up * 0.2f, out hit, 0.2f)) {
            if (hit.collider.tag == "Spring") {
                springConnected = true;
            }
        } else {
            springConnected = false;
        }
        Debug.DrawRay(transform.position, -controller.board.transform.up * 0.2f);
    }

    void Update()
    {
    }
}
