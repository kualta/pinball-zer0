﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumperController : MonoBehaviour
{
    public Material ActiveMaterial;
    public Material IdleMaterial;

    void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Ball"){
            other.rigidbody.AddForce(1100 * other.GetContact(0).normal);
            GetComponent<Renderer>().material = ActiveMaterial;
        }
    }

    void OnCollisionExit(Collision other) {
        GetComponent<Renderer>().material = IdleMaterial;
    }
}
