﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class FlipperController : MonoBehaviour
{
    public float restPosition = 0f;
    public float activePosition = 45f;
    public float flipperDamper = 150f;
    public float power = 3000f;
    public string inputName;

    HingeJoint hinge;
    public JointSpring spring;

    void Start() {
        hinge = GetComponent<HingeJoint>();
        hinge.useSpring = true;
    }

    void Update() {
        JointSpring spring = new JointSpring();

        spring.spring = power;
        spring.damper = flipperDamper;

        if (Input.GetButton(inputName)) {
            spring.targetPosition = activePosition;
        } else {
            spring.targetPosition = restPosition;
        }

        hinge.spring = spring;
        // hinge.useLimits = true;
    }

    // public Vector3 fromVector;
    // public Vector3 toVector;
    // public float speed = 500f;
    // public Vector3 Euler;
    // public float timer;

    // Quaternion fromRotation;
    // Quaternion toRotation;

    // public delegate void activeState();
    // public activeState ActiveState;

    // public void setState(Action state) {
    //     ActiveState = new activeState(state);
    // }

    // public void Activate() {
    //     transform.Rotate(0f, speed * Time.deltaTime, 0f, Space.Self);
    //     timer += speed * Time.deltaTime;
    //     // transform.rotation = Quaternion.LookRotation(new Vector3(), transform.forward);
    //     // transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, Time.deltaTime * speed);
    // }

    // public void Deactivate() {
    //     // transform.rotation = Quaternion.LookRotation(fromVector, transform.forward);
    //     transform.Rotate(0f, -speed * Time.deltaTime, 0f, Space.Self);
    //     timer -= speed * Time.deltaTime;
    //     // transform.rotation = fromRotation;
    //     // transform.rotation = Quaternion.Lerp(transform.rotation, fromRotation, Time.deltaTime * speed);
    // }

    // public void Idle() {

    // }

    // void Start() {
    //     // fromRotation.SetFromToRotation(fromVector, toVector);
    //     // toRotation.SetFromToRotation(toVector, fromVector);
    //     fromRotation = Quaternion.LookRotation(fromVector);
    //     toRotation = Quaternion.LookRotation(toVector);
    //     setState(Idle);
    // }
    // void FixedUpdate() {
    //     ActiveState();
    // }

    // void Update() {
    //     Debug.DrawRay(transform.position, Vector3.forward);
    //     // transform.localEulerAngles.y = Mathf.Clamp(transform.localEulerAngles.y, 0, 90);
    //     Euler.y = transform.localEulerAngles.y;
    //     Euler.x = transform.localEulerAngles.x;
    //     Euler.z = transform.localEulerAngles.z;

    // }
}
