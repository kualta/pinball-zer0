﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    internal InputController input;

    public GameObject spawn;
    public GameObject ball;
    public GameObject spring;
    public GameObject board;

    public int lives;
    public float springMinScale;
    public GameObject currentBall;
    bool spawnKicked = false;
    Coroutine compress;
    Coroutine release;

    void Start()
    {
        currentBall = null;
        spawnKicked = false;
    }

    void Update()
    {
    }

    private IEnumerator ReleaseString() {
        for (float i = spring.transform.localScale.z; i < 1.744f; i += 0.1f * Time.deltaTime * 100) {
            if (currentBall.GetComponent<BallController>().springConnected) {
                currentBall.GetComponent<Rigidbody>().AddForce(board.transform.up * 200);
            }
            spring.transform.localScale += new Vector3(0, 0, 0.1f) * Time.deltaTime * 100;
            yield return null;
        }
    }

    private IEnumerator CompressString() {
        for (float i = spring.transform.localScale.z; i > springMinScale; i -= 0.1f) {
            if (!spawnKicked) {
                if (spring.transform.localScale.z > springMinScale) {
                    spring.transform.localScale -= new Vector3(0, 0, 0.1f);
                }
            }
            yield return new WaitForSeconds(.1f);
        }
        yield return null;
    }

    public void SpawnBall() {
        currentBall = Instantiate(ball, spawn.transform.position, Quaternion.identity);
        spawnKicked = false;
        lives -= 1;
    }
}
