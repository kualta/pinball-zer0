﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyFieldController : MonoBehaviour
{
    public GameObject GameController;
    GameController controller;

    void Start() {
        controller = GameController.GetComponent<GameController>();
    }

    void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Ball") {
            Destroy(other.gameObject);
            controller.currentBall = null;
        }
    }
}
