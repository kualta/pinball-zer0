﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField]
    internal GameController controller;

    [SerializeField]
    GameObject springObj;

    [SerializeField]
    GameObject flipperLeftObj;

    [SerializeField]
    GameObject flipperRightObj;


    SpringController spring;
    FlipperController flipperL;
    FlipperController flipperR;

    void Start()
    {
        flipperL = flipperLeftObj.GetComponent<FlipperController>();
        flipperR = flipperRightObj.GetComponent<FlipperController>();
        spring = springObj.GetComponent<SpringController>();
    }

    void Update()
    {
        if (Input.GetButton("Start") && controller.lives > 0 && controller.currentBall == null) {
            controller.SpawnBall();
        }

        if (!Input.GetButton("Kick")) {
            spring.setState(spring.Idle);
        }

        // if (Input.GetButton("FlipperLeft") && flipperL.timer <= 75) {
        //     flipperL.setState(flipperL.Activate);
        //     Debug.Log("LEFT flipper ACTIVATED");
        // } else if (Input.GetButton("FlipperLeft") && flipperL.timer > 75) {
        //     flipperL.setState(flipperL.Idle);
        // }

        // if (!Input.GetButton("FlipperLeft") && flipperL.timer > 0) {
        //     flipperL.setState(flipperL.Deactivate);
        //     Debug.Log("LEFT flipper DEACTIVATED");
        // } else if (!Input.GetButton("FlipperLeft") && flipperL.timer <= 0) {
        //     flipperL.setState(flipperL.Idle);
        // }

        // if (Input.GetButtonDown("FlipperRight")) {
        //     flipperR.setState(flipperR.Activate);
        //     Debug.Log("RIGHT flipper ACTIVATED");
        // }

        // if (Input.GetButtonUp("FlipperRight")) {
        //     flipperR.setState(flipperR.Deactivate);
        //     Debug.Log("RIGHT flipper DEACTIVATED");
        // }



        if (Input.GetButton("Kick") && springObj.transform.localScale.z >= spring.springMinScaleZ) {
            spring.setState(spring.Compress);
        } else if (Input.GetButton("Kick") && springObj.transform.localScale.z <= spring.springMinScaleZ) {
            spring.setState(spring.Idle);
        }

        if (!Input.GetButton("Kick") && springObj.transform.localScale.z <= spring.springMaxScaleZ) {
            spring.setState(spring.Release);
            if (controller.currentBall.GetComponent<BallController>().springConnected) {
                controller.currentBall.GetComponent<Rigidbody>().AddForce(spring.springPower * controller.board.transform.up);
            }
        }
    }
}
